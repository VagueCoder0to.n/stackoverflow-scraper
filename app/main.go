package main

import (
	"fmt"

	"gitlab.com/VagueCoder0to.n/StackOverflow-Scraper/app/config"
	"gitlab.com/VagueCoder0to.n/StackOverflow-Scraper/app/scraper"
)

func main() {
	fmt.Println("Welcome")

	url := config.TagURL

	scraper.Scrape(url)
}
