package scraper

import (
	"fmt"
	"net/http"
	"net/url"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type Record struct {
	question    Question
	tags        []Tag
	voteCount   int
	answerCount int
	viewCount   int
	createdTime time.Time
}

type Question struct {
	name string
	url  string
}

type Tag struct {
	name string
	url  string
}

var (
	err error
)

func Scrape(u string) error {
	res, err := http.Get(u)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return err
	}

	var parsedUrl *url.URL
	parsedUrl, err = url.Parse(u)
	if err != nil {
		return err
	}
	parsedUrl.Path = ""

	var item Record
	var items []Record
	doc.Find("#questions div.mln24").Each(func(i int, s *goquery.Selection) {
		item = Record{}
		if i < 100 {
			item.scrapeQuestion(s, *parsedUrl)
			item.scrapeTags(s, *parsedUrl)
			item.scrapeVoteCount(s)
			item.scrapeAnswerCount(s)
			item.scrapeViewCount(s)
			items = append(items, item)
		}
	})

	// fmt.Printf("%+v\n", items)
	for _, item = range items {
		fmt.Println(item)
	}
	return nil
}

func (r *Record) scrapeQuestion(s *goquery.Selection, u url.URL) {
	t := s.Find("a.question-hyperlink")

	r.question.name = t.Text()

	p, ok := t.Attr("href")
	if ok {
		u.Path = path.Join(u.Path, p)
		r.question.url = u.String()
	}
}

func (r *Record) scrapeTags(s *goquery.Selection, u url.URL) {
	var iterUrl url.URL
	s.Find("a.post-tag").Each(func(i int, t *goquery.Selection) {
		var tag Tag
		iterUrl = u

		tag.name = t.Text()

		p, ok := t.Attr("href")
		if ok {
			iterUrl.Path = path.Join(iterUrl.Path, p)
			tag.url = iterUrl.String()
		}

		r.tags = append(r.tags, tag)
	})
}

func (r *Record) scrapeVoteCount(s *goquery.Selection) {
	votes := s.Find("div.votes span.vote-count-post strong").Text()
	if len(votes) == 0 {
		r.voteCount = 0
		return
	}

	r.voteCount, err = strconv.Atoi(votes)
	if err != nil {
		r.voteCount = 0
	}
}

func (r *Record) scrapeAnswerCount(s *goquery.Selection) {
	answers := s.Find("div.stats div.status strong").Text()
	if len(answers) == 0 {
		r.answerCount = 0
		return
	}

	answers = strings.TrimSpace(answers)
	r.answerCount, err = strconv.Atoi(answers)
	if err != nil {
		r.answerCount = 0
	}
}

func (r *Record) scrapeViewCount(s *goquery.Selection) {
	views := s.Find("div.statscontainer div.views").Text()
	if len(views) == 0 {
		r.viewCount = 0
		return
	}

	pattern := regexp.MustCompile(`[0-9]+`)
	views = string(pattern.Find([]byte(views)))

	r.viewCount, err = strconv.Atoi(views)
	if err != nil {
		r.viewCount = 0
	}
}

func (r *Record) scrapeCreatedTime(s *goquery.Selection) error {
	return nil
}
