package config

import "os"

var (
	ProjectRoot string
	TagURL      string
)

func init() {
	ProjectRoot = os.Getenv("PROJECT_ROOT")
	TagURL = os.Getenv("TAG_URL")
}
